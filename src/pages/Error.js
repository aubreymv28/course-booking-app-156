// Identify the components that we will use to build the page
import Header from './../components/Banner';

// Pass down 'property'
const data = {
	title: '404 Page Not Found',
	content: 'The page you are looking for does not exist.'
}


export default function ErrorPage(){
	return(
		<Header bannerData={data}/>
	);
};